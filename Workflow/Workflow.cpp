#include "Worker.h"

typedef struct _SchemeBlock
{
	int id;
	string command;
	string text1;
	string text2;
} SchemeBlock;

class Parser
{
private:
	vector<SchemeBlock> scheme;
	vector<int> WorkFlow;
	string filename;
	string input;
	string output;
	int flag;
	void ExecuteBlock(Worker *w);
public:
	Parser() { flag = 0; }
	void SetFile(string in, string out);
	void SetFlag(int c);
	void SetFileName(string name) { filename = name; }
	void ParseFile();
	void ParseFile(string name);
	void ExecuteWorkflow();
};
void Parser::SetFile(string in, string out)
{
	input = in;
	output = out;
}
void Parser::SetFlag(int c)
{
	flag = c;
}
void Parser::ParseFile()
{
	string word, line;
	SchemeBlock s1;
	ifstream infile;
	/*try
	{
		char *ch = NULL;
		ch[25] = 'X';
	}
	catch (...)
	{
		cerr << "ch!!!!!!" << endl;
	}*/
	infile.exceptions(ifstream::failbit | ifstream::badbit);
	try {
		infile.open(filename);
		getline(infile, line);
		if (line != "desc") throw 1;
		while (true)
		{
			s1.text1 = "";
			s1.text2 = "";
			infile >> word;
			if (word == "csed") break;
			istringstream iss;
			iss.str(word);
			iss >> s1.id;
			infile >> word;
			if (word != "=") throw 1;
			infile >> s1.command;
			if (s1.command != "sort")
				infile >> s1.text1;
			if (s1.command == "replace")
				infile >> s1.text2;
			scheme.push_back(s1);
		}
		while (true)
		{
			infile >> word;
			istringstream iss;
			iss.str(word);
			int val;
			iss >> val;
			WorkFlow.push_back(val);
			if (infile.eof()) return;
			infile >> word;
			if (word != "->") throw 1;
		}
	}
	catch (ifstream::failure e) {
		cerr << "Exception opening/reading/closing file\n";
		system("pause");
		exit(EXIT_FAILURE);
	}
	catch (int i)
	{
		cerr << "Wrong file format" << endl;
		exit(EXIT_FAILURE);
	}
}

void Parser::ParseFile(string name)
{
	filename = name;
	ParseFile();
}

void Parser::ExecuteWorkflow()
{
	size_t j, v1;
	SchemeBlock s1;
	list <string> text;
	/*
	flag == 0 -i and -o are not set
	flag == 1 -i and -o are set
	flag == 2 -i is set
	flag == 3 -o is set
	*/
	if ((flag == 1) || (flag == 2))
	{
		ReadFileBlock block;
		block.SetText(text);
		block.SetArg1(input);
		ExecuteBlock(&block);
		block.GetText(text);
	}
	for (j = 0; j < WorkFlow.size(); j++)
	{
		v1 = WorkFlow[j];
		for (size_t i = 0; i < scheme.size(); i++)
		{
			s1 = scheme[i];
			if (s1.id == v1)
			{
				try {
					if (((flag == 1) || (flag == 2)) && (s1.command == "readfile")) throw 1;
					if ((j == 0) && (flag != 1) && (flag != 2) && (s1.command != "readfile")) throw 1;
					if (((flag == 1) || (flag == 3)) && (s1.command == "writefile")) throw 1;
					if ((j == WorkFlow.size() - 1) && (flag != 1) && (flag != 3) && (s1.command != "writefile")) throw 1;
					if ((j > 0) && (s1.command == "readfile")) throw 1;
					if ((j < WorkFlow.size() - 1) && (s1.command == "writefile")) throw 1;
					if (s1.command == "replace")
					{
						ReplaceBlock block;
						block.SetText(text);
						block.SetArg1(s1.text1);
						block.SetArg2(s1.text2);
						ExecuteBlock(&block);
						block.GetText(text);
					}
					else if (s1.command == "grep")
					{
						GrepBlock block;
						block.SetText(text);
						block.SetArg1(s1.text1);
						ExecuteBlock(&block);
						block.GetText(text);
					}
					else if (s1.command == "sort")
					{
						SortBlock block;
						block.SetText(text);
						ExecuteBlock(&block);
						block.GetText(text);
					}
					else if (s1.command == "readfile")
					{
						ReadFileBlock block;
						block.SetText(text);
						block.SetArg1(s1.text1);
						ExecuteBlock(&block);
						block.GetText(text);
					}
					else if (s1.command == "writefile")
					{
						WriteFileBlock block;
						block.SetText(text);
						block.SetArg1(s1.text1);
						ExecuteBlock(&block);
					}
					else if (s1.command == "dump")
					{
						DumpBlock block;
						block.SetText(text);
						block.SetArg1(s1.text1);
						ExecuteBlock(&block);
						block.GetText(text);
					}
					else throw 1;
				}
				catch (int i)
				{
					cerr << "Wrong file format" << endl;
					system("pause");
					exit(EXIT_FAILURE);
				}
			}
		}
	}
	if ((flag == 1) || (flag == 3))
	{
		WriteFileBlock block;
		block.SetText(text);
		block.SetArg1(output);
		ExecuteBlock(&block);
	}
}

void Parser::ExecuteBlock(Worker *w)
{
	w->Execute();
}

void main(int argc, char *argv[])
{
	Parser p;
	try
	{
		if (argc == 6)
		{
			if ((string(argv[2]) != "-i") || (string(argv[4]) != "-o"))
				throw 2;
			p.SetFile(argv[3], argv[5]);
			p.SetFlag(1);
		}
		else if (argc == 4)
		{
			if ((string(argv[2]) != "-i") && (string(argv[2]) != "-o"))
				throw 2;
			if (string(argv[2]) == "-i") {
				p.SetFile(argv[3], "");
				p.SetFlag(2);
			}
			else
			{
				p.SetFile("", argv[3]);
				p.SetFlag(3);
			}
		}
		else if (argc != 2) throw 2;
	}
	catch (int i)
	{   
		cout << "Wrong input" << endl;
		exit(EXIT_FAILURE);
	}
	p.ParseFile(argv[1]);
	p.ExecuteWorkflow();
}