#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <process.h>
#include <fstream>
#include <sstream>
using namespace std;

class Worker
{
protected:
	list <string> text;
	string arg1;
	string arg2;
public:
	virtual ~Worker() {}
	virtual void Execute() = 0;
	void SetText(const list <string> &txt);
	void GetText(list <string> &txt);
	void SetArg1(string arg) { arg1 = arg; };
	void SetArg2(string arg) { arg2 = arg; };
};

class ReadFileBlock: public Worker
{
public:
	virtual ~ReadFileBlock() {}
	virtual void Execute();
};
class WriteFileBlock : public Worker
{
public:
	virtual ~WriteFileBlock() {}
	virtual void Execute();
};
class GrepBlock : public Worker
{
public:
	virtual ~GrepBlock() {}
	virtual void Execute();
};
class SortBlock : public Worker
{
public:
	virtual ~SortBlock() {}
	virtual void Execute();
};
class ReplaceBlock : public Worker
{
public:
	virtual ~ReplaceBlock() {}
	virtual void Execute();
};
class DumpBlock : public Worker
{
public:
	virtual ~DumpBlock() {}
	virtual void Execute();
};
