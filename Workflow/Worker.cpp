#include "Worker.h"

void Worker::SetText(const list <string> &txt)
{
	text = txt;
}
void Worker::GetText(list <string> &txt)
{
	txt = text;
}

void ReadFileBlock::Execute()
{
	ifstream infile(arg1);
	string s1;
	while (!infile.eof())
	{
		getline(infile,s1);
		text.push_back(s1);
	}
}
void WriteFileBlock::Execute()
{
	ofstream outfile(arg1);
	string s1;
	list<string>::iterator iter;
	for (iter=text.begin(); iter!= text.end(); iter++)
	{
		s1 = *iter;
		outfile << s1.c_str() << endl;
	}
}

/*pos = s1.find("word");
string s2 = s1.substr(0, pos) + string("word 2") + s1.substr(pos + strlen("word"), s1.lenghth());
*/
void GrepBlock::Execute()
{
	string s1;
	int pos = -1;
	list <string> new_text;
	list<string>::iterator iter;
	for (iter = text.begin(); iter != text.end(); iter++)
	{
		s1 = *iter;
		pos = s1.find(arg1);
		if (pos != -1) new_text.push_back(s1);
	}
	text = new_text;
}
void SortBlock::Execute()
{
	text.sort();
}
void ReplaceBlock::Execute()
{
	string s1;
	int pos = -1;
	int size;
	size = arg1.size();
	list <string> new_text;
	list<string>::iterator iter;
	for (iter = text.begin(); iter != text.end(); iter++)
	{
		s1 = *iter;
		pos = s1.find(arg1);
		while (pos != -1)
		{
			s1.replace(pos, size, arg2);
			pos = -1;
			pos = s1.find(arg1);
		}
		new_text.push_back(s1);
	}
	text = new_text;
}
void DumpBlock::Execute()
{
	ofstream outfile;
	outfile.exceptions(ofstream::failbit | ofstream::badbit);
	try {
		outfile.open(arg1);
		string s1;
		list<string>::iterator iter;
		for (iter = text.begin(); iter != text.end(); iter++)
		{
			s1 = *iter;
			outfile << s1.c_str() << endl;
		}
	}
	catch (ofstream::failure e) {
		cerr << "Exception opening/reading/closing file\n";
	}
}